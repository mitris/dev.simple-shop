<?php

Route::get('/', 'CategoryController@index');
Route::get('/category/{id?}/{slug?}', 'CategoryController@index')->name('category.index');

Route::get('/product/show/{id}/{slug?}', 'ProductController@show')->name('product.show');
Route::get('/product/attributes/{id}', 'ProductController@attributes')->name('product.attributes');