<?php

use App\Product;
use Illuminate\Database\Seeder;

use App\Category;
use App\Attribute;
use App\Specification;

class SampleDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category_phones = Category::create([
            'name' => 'Phones',
            'slug' => str_slug('Phones'),
        ]);
        $category_tv = Category::create([
            'name' => 'TV',
            'slug' => str_slug('TV'),
        ]);
        $category_notebooks = Category::create([
            'name' => 'Notebooks',
            'slug' => str_slug('Notebooks'),
        ]);


        $attribute_capacity = Attribute::create([
            'name' => 'Capacity',
            'use_for_filter' => true,
        ]);
        $attribute_diagonal = Attribute::create([
            'name' => 'Diagonal',
            'use_for_filter' => false,
        ]);
        $attribute_color = Attribute::create([
            'name' => 'Color',
            'use_for_filter' => true,
        ]);

        $specification_processor = Specification::create([
            'name' => 'Processor',
            'use_for_filter' => true,
        ]);

        $specification_size = Specification::create([
            'name' => 'Size',
            'use_for_filter' => true,
        ]);
        $specification_weight = Specification::create([
            'name' => 'Weight',
            'use_for_filter' => true,
        ]);


        $category_phones->attributes()->attach([
            $attribute_capacity->id,
            $attribute_color->id,
        ]);
        $category_tv->attributes()->attach([
            $attribute_diagonal->id,
            $attribute_color->id,
        ]);
        $category_notebooks->attributes()->attach([
            $attribute_capacity->id,
            $attribute_diagonal->id,
        ]);


        $category_phones->specifications()->attach([
            $specification_processor->id,
            $specification_weight->id,
        ]);
        $category_tv->specifications()->attach([
            $specification_size->id,
            $specification_weight->id,
        ]);
        $category_notebooks->specifications()->attach([
            $specification_size->id,
            $specification_weight->id,
        ]);

        $product = Product::create([
            'category_id' => $category_phones->id,
            'sku' => 'AI7_32B',
            'name' => 'iPhone 7 32 Gb Black',
            'slug' => str_slug('iPhone 7 32 Gb Black'),
            'price' => 700,
            'attributes' => [
                $attribute_capacity->id => '32 Gb',
                $attribute_color->id => 'Black',
            ],
            'specifications' => [
                $specification_processor->id => 'A11',
                $specification_weight->id => 100,
            ],
        ]);

        $colors = ['Gray', 'Space Gray', 'Red', 'Gold', 'Rose Gold'];

        for ($i = 0; $i <= 5; $i++) {
            $capacity = rand(64, 256);
            $color = $colors[rand(0, count($colors) - 1)];

            $name = 'iPhone 7 ' . $capacity . ' Gb ' . $color;

            Product::create([
                'category_id' => $category_phones->id,
                'parent_id' => $product->id,
                'sku' => 'AI7_' . $capacity,
                'name' => $name,
                'slug' => str_slug($name),
                'price' => rand(650, 900),
                'attributes' => [
                    $attribute_capacity->id => $capacity . ' Gb',
                    $attribute_color->id => $color,
                ],
                'specifications' => [
                    $specification_processor->id => 'A11',
                    $specification_weight->id => 100,
                ],
            ]);
        }

        $faker = \Faker\Factory::create();

        for ($i = 0; $i <= 15; $i++) {
            $name = $faker->company;
            Product::create([
                'category_id' => rand(1, 3),
                'sku' => rand() . '_' . rand(),
                'name' => $name,
                'slug' => str_slug($name),
                'price' => rand(650, 900),
            ]);
        }

        $category_tv->recountProducts();
        $category_phones->recountProducts();
        $category_notebooks->recountProducts();
    }
}
