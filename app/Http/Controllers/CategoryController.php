<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index($id = null, $slug = null)
    {
        $query = Product::query();
        if ($id) {
            $query->where('category_id', $id);
        }
        if (config('shop.show_only_basic_products')) {
            $query->whereNull('parent_id');
        }

        $pager = $query->paginate(6);

        return view('category.index', compact('pager'));
    }

}
