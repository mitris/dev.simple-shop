<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function show($id, $slug = null)
    {
        $product = Product::findOrFail($id);

        $variants = Product::where('parent_id', $product->id)->get();

        return view('product.show', compact('product', 'variants'));
    }

    public function attributes($id)
    {
        $product = Product::findOrFail($id);

        return view('product._variants', compact('product'));
    }

}
