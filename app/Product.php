<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    public $fillable = ['category_id', 'parent_id', 'sku', 'name', 'price', 'attributes', 'specifications'];

    public $timestamps = false;

    public $casts = [
        'attributes' => 'json',
        'specifications' => 'json',
    ];

    public function parent()
    {
        return $this->hasOne('App\Product', 'id', 'parent_id');
    }

}
