<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{

    public $fillable = ['name', 'use_for_filter'];

    public $timestamps = false;

}
