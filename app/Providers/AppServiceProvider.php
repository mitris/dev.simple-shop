<?php

namespace App\Providers;

use App\Category;
use App\Attribute;
use App\Specification;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (env('APP_DEBUG')) {
            Cache::flush();
        }

        Cache::rememberForever('categories', function () {
            return Category::getFormattedForCache();
        });

        Cache::rememberForever('attributes', function () {
            return Attribute::all()->keyBy('id')->toArray();
        });

        Cache::rememberForever('specifications', function () {
            return Specification::all()->keyBy('id')->toArray();
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
