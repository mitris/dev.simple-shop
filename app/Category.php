<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    public $fillable = ['name', 'products_count'];

    public $timestamps = false;

    public function attributes()
    {
        return $this->belongsToMany('App\Attribute', 'category_attribute');
    }

    public function specifications()
    {
        return $this->belongsToMany('App\Specification', 'category_specification');
    }

    public static function getFormattedForCache()
    {
        $categories = self::with(['attributes', 'specifications'])->get()->keyBy('id')->toArray();

        foreach ($categories as $category) {
            $attributes = [];
            foreach ($category['attributes'] as $attribute) {
                $attributes[$attribute['id']] = $attribute;
            }

            $specifications = [];
            foreach ($category['specifications'] as $specification) {
                $specifications[$specification['id']] = $specification;
            }

            $category['attributes'] = $attributes;
            $category['specifications'] = $specifications;
        }

        return $categories;
    }

    public function recountProducts()
    {
        $this->products_count = Product::where('category_id', $this->id)->count();

        $this->save();
    }

}
