<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specification extends Model
{

    public $fillable = ['name', 'use_for_filter'];

    public $timestamps = false;

}
