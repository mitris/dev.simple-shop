<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Simple Shop</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
<div class="container">
    <h4 class="text-center">Simple Shop</h4>
    <hr>
    <div class="row">
        <div class="col-sm-3">
            <div class="list-group">
                <a href="/" class="list-group-item list-group-item-action">All</a>
                @foreach(cache('categories') as $category)
                    <a href="{{ route('category.index', ['id' => $category['id'], 'slug' => $category['slug']]) }}" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                        {{ $category['name'] }}
                        <span class="badge badge-primary badge-pill">{{ $category['products_count'] }}</span>
                    </a>
                @endforeach
            </div>
        </div>
        <div class="col-sm-9">
            @yield('content')
        </div>
    </div>
</div>


@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
@show


</body>
</html>