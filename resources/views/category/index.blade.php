@extends('layouts.app')

@section('content')
    @if(request('id') && isset(cache('categories')[request('id')]))
        <h5>
            {{ cache('categories')[request('id')]['name'] }}
        </h5>
        <hr>
    @endif
    <div class="row">
        @foreach($pager as $item)
            <div class="col-sm-6">
                <div class="card mb-3">
                    <div class="card-body">
                        <div>
                            <a href="{{ route('product.show', ['id' => $item->id, 'slug' => $item->slug]) }}">{{ $item->name }}</a>
                        </div>
                        <small>
                            <b>SKU:</b>
                            {{ $item->sku }}
                        </small>
                        <div class="card-text mb-3">
                            <b>Price:</b> {{ number_format($item->price, 2) }}
                        </div>
                        <a href="{{ route('product.show', ['id' => $item->id, 'slug' => $item->slug]) }}" class="btn btn-sm btn-outline-secondary">Details</a>
                    </div>
                </div>
            </div>
            @if($loop->index % 2 == 1)
                </div><div class="row">
            @endif
        @endforeach
    </div>
    {!! $pager->appends(request()->all())->links() !!}
@endsection