@if($product->attributes)
    <table class="table table-bordered table-hover">
        @foreach($product->attributes as $spec_id => $value)
            @if(isset(cache('attributes')[$spec_id]))
                <tr>
                    <th style="width: 25%">
                        {{ cache('attributes')[$spec_id]['name'] }}
                    </th>
                    <td>
                        {{ $value }}
                    </td>
                </tr>
            @endif
        @endforeach
    </table>
@endif