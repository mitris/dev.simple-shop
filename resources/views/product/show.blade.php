@extends('layouts.app')

@section('content')
    <h5>{{ $product->name }}</h5>
    <hr>
    <div>
        <b>SKU:</b> {{ $product->sku }}
    </div>
    <div class="mb-2">
        <b>Price:</b> {{ $product->price }}
    </div>

    @if(count($variants))
        <div class="card mb-3">
            <div class="card-body">
                <b>Variants:</b>
                <br>
                @foreach($variants as $variant)
                    <a href="{{ route('product.attributes', ['id' => $variant->id]) }}" data-action="variant-load-attributes" data-id="{{ $variant->id }}" class="btn btn-outline-secondary d-inline-block mt-1">{{ $variant->name }}</a>
                @endforeach
            </div>
        </div>
    @endif

    <div data-container="variant-attributes">
        @include('product._variants', ['product' => $product])
    </div>
    @if($product->specifications)
        <table class="table table-bordered table-hover">
            @foreach($product->specifications as $spec_id => $value)
                @if(isset(cache('specifications')[$spec_id]))
                    <tr>
                        <th style="width: 25%">
                            {{ cache('specifications')[$spec_id]['name'] }}
                        </th>
                        <td>
                            {{ $value }}
                        </td>
                    </tr>
                @endif
            @endforeach
        </table>
    @endif

    @section('js')
        @parent
        <script>
            $(function () {
                var variants_container = $('[data-container="variant-attributes"]'),
                    variants_links = $('[data-action="variant-load-attributes"]');

                $(document).on('click', '[data-action="variant-load-attributes"]', function (e) {
                    e.preventDefault();

                    var el = $(this);

                    variants_links.removeClass('active');

                    $.ajax(el.attr('href'))
                        .success(function (html) {
                            variants_container.html(html);
                            el.addClass('active');
                        })
                        .error(function (err) {
                            console.log(err)
                        });
                });
            });
        </script>
    @endsection
@endsection